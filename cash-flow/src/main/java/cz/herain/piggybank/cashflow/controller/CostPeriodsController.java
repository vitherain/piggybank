package cz.herain.piggybank.cashflow.controller;

import com.google.gson.Gson;
import cz.herain.piggybank.common.controller.PortletControllerAdvice;
import cz.herain.piggybank.common.dto.CostMonthDTO;
import cz.herain.piggybank.common.dto.CostYearDTO;
import cz.herain.piggybank.common.dto.CostYearWithMonthsDTO;
import cz.herain.piggybank.common.exception.ValidationException;
import cz.herain.piggybank.common.service.CostMonthService;
import cz.herain.piggybank.common.service.CostYearService;
import cz.herain.piggybank.common.util.ContextUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@Controller
@RequestMapping("VIEW")
public class CostPeriodsController extends PortletControllerAdvice {

    @Autowired
    private ContextUtil contextUtil;

    @Autowired
    private CostYearService costYearService;

    @Autowired
    private CostMonthService costMonthService;

    @ResourceMapping("usersCostPeriods")
    public void getUsersCostYears(ResourceRequest request, ResourceResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        Gson gson = new Gson();

        long userId = contextUtil.getCurrentPbUserId();

        List<CostYearWithMonthsDTO> costYears = costYearService.getAllWithMonthsByUserIdSortedByYearDesc(userId);
        String costYearsJson = gson.toJson(costYears);

        out.print(costYearsJson);
    }

    @ResourceMapping(value = "submitCostYear")
    public void submitCostYear(@ModelAttribute(value = "costYear") @Valid CostYearDTO costYear, BindingResult result,
                               ResourceRequest actionRequest, ResourceResponse actionResponse) {
        if (result.hasErrors()) {
            throw new ValidationException(result);
        }

        costYearService.saveOrUpdate(costYear);
    }

    @ResourceMapping(value = "submitCostMonth")
    public void submitCostMonth(@ModelAttribute(value = "costMonth") @Valid CostMonthDTO costMonth, BindingResult result,
                                ResourceRequest actionRequest, ResourceResponse actionResponse) {
        if (result.hasErrors()) {
            throw new ValidationException(result);
        }

        costMonthService.saveOrUpdate(costMonth);
    }
}
