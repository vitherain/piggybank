package cz.herain.piggybank.cashflow.controller;

import cz.herain.piggybank.cashflow.util.Views;
import cz.herain.piggybank.common.controller.PortletControllerAdvice;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

@Controller
@RequestMapping("VIEW")
public class HomeController extends PortletControllerAdvice {

	@RenderMapping
	public String cashFlowView() {
		return Views.CASH_FLOW;
	}
}