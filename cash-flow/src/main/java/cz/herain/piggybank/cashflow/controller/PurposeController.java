package cz.herain.piggybank.cashflow.controller;

import com.liferay.portal.kernel.servlet.SessionErrors;
import cz.herain.piggybank.cashflow.util.Views;
import cz.herain.piggybank.common.controller.PortletControllerAdvice;
import cz.herain.piggybank.common.dto.PurposeDTO;
import cz.herain.piggybank.common.entity.PbUser;
import cz.herain.piggybank.common.service.PurposeService;
import cz.herain.piggybank.common.util.ContextUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("VIEW")
public class PurposeController extends PortletControllerAdvice {

    @Autowired
    private ContextUtil contextUtil;

    @Autowired
    private PurposeService purposeService;

    @RenderMapping(params = "render=definePurposeCodeTable")
    public String defineCodeTableView(Model model) {
        List<PurposeDTO> purposes = purposeService.getAllForCurrentUserSortedByNameAsc();
        model.addAttribute("purposes", purposes);

        PurposeDTO purposeDTO = new PurposeDTO();
        model.addAttribute("purpose", purposeDTO);

        return Views.DEFINE_PURPOSE_CODE_TABLE_VIEW;
    }

    @RenderMapping(params = "render=editPurpose")
    public String editPurposeRender(@RequestParam(value = "purposeId") Long purposeId,
            Model model) {
        List<PurposeDTO> purposes = purposeService.getAllForCurrentUserSortedByNameAsc();
        model.addAttribute("purposes", purposes);

        PurposeDTO purposeDTO = purposeService.getOne(purposeId);
        model.addAttribute("purpose", purposeDTO);

        return Views.DEFINE_PURPOSE_CODE_TABLE_VIEW;
    }

    @ActionMapping(value = "submitPurpose")
    public void submitPurpose(@ModelAttribute(value = "purpose") @Valid PurposeDTO purposeDTO, BindingResult result,
            ActionRequest actionRequest, ActionResponse actionResponse) {
        if (result.hasErrors()) {
            SessionErrors.add(actionRequest, "error-sample");
            actionResponse.setRenderParameter("render", Views.DEFINE_PURPOSE_CODE_TABLE_VIEW);
            return;
        }

        if (purposeDTO.canNotBeSetAsDefault()) {
            purposeDTO.setDefaultForCurrentUser(false);
        }
        purposeService.saveOrUpdate(purposeDTO);
        actionResponse.setRenderParameter("render", Views.DEFINE_PURPOSE_CODE_TABLE_VIEW);
    }

    @ActionMapping(value = "setPurposeAsDefault")
    public void setPurposeAsDefault(@RequestParam(value = "purposeId", required = true) Long purposeId,
            ActionResponse actionResponse) {
        PurposeDTO purposeDTO = purposeService.getOne(purposeId);
        if (purposeDTO.canBeSetAsDefault()) {
            purposeDTO.setDefaultForCurrentUser(true);
        } else {
            purposeDTO.setDefaultForCurrentUser(false);
        }
        purposeService.saveOrUpdate(purposeDTO);

        actionResponse.setRenderParameter("render", Views.DEFINE_PURPOSE_CODE_TABLE_VIEW);
    }

    @ActionMapping(value = "activatePurpose")
    public void activatePurpose(@RequestParam(value = "purposeId", required = true) Long purposeId,
            ActionResponse actionResponse) {
        PurposeDTO purposeDTO = purposeService.getOne(purposeId);
        purposeDTO.setActive(true);
        purposeService.saveOrUpdate(purposeDTO);
        actionResponse.setRenderParameter("render", Views.DEFINE_PURPOSE_CODE_TABLE_VIEW);
    }

    @ActionMapping(value = "deactivatePurpose")
    public void deactivatePurpose(@RequestParam(value = "purposeId", required = true) Long purposeId,
            ActionResponse actionResponse) {

        removeDefaultPurposeForCurrentUser();

        PurposeDTO purposeDTO = purposeService.getOne(purposeId);
        purposeDTO.setActive(false);
        purposeDTO.setDefaultForCurrentUser(false);

        purposeService.saveOrUpdate(purposeDTO);

        actionResponse.setRenderParameter("render", Views.DEFINE_PURPOSE_CODE_TABLE_VIEW);
    }

    private void removeDefaultPurposeForCurrentUser() {
        PbUser pbUser = contextUtil.getCurrentPbUser();

        pbUser.setDefaultPurpose(null);
        pbUserRepository.save(pbUser);
    }
}
