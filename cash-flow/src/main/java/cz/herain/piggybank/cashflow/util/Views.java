package cz.herain.piggybank.cashflow.util;

public class Views {

    public static final String CASH_FLOW = "cashFlow";
    public static final String DEFINE_PURPOSE_CODE_TABLE_VIEW = "definePurposeCodeTable";

    private Views() {}
}
