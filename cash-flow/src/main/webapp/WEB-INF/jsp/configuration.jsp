<%@ page import="com.liferay.portal.kernel.util.Constants" %>
<%@ page import="com.liferay.portal.kernel.util.GetterUtil" %>
<%@ page import="com.liferay.portal.kernel.util.StringPool" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0" %>
<%@ taglib prefix="aui" uri="http://liferay.com/tld/aui" %>
<%@ taglib prefix="liferay-portlet" uri="http://liferay.com/tld/portlet" %>

<portlet:defineObjects />

This is the <b>cash-flows</b> configuration page.<br />

<%
    boolean testKey_cfg = GetterUtil.getBoolean(portletPreferences.getValue("testKey", StringPool.TRUE));
%>

<%--<portlet:actionURL portletConfiguration="true" var="configurationURL" />--%>
<liferay-portlet:actionURL portletConfiguration="true" var="configurationURL" />

<aui:form action="<%= configurationURL %>" method="post" name="fm">
    <aui:input name="<%= Constants.CMD %>" type="hidden" value="<%= Constants.UPDATE %>" />

    <!-- Preference control goes here -->

    <aui:button-row>
        <aui:input name="preferences--testKey--" type="checkbox" value="<%= testKey_cfg %>" />
        <aui:button type="submit" />
    </aui:button-row>

</aui:form>

