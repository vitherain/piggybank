<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@include file="includes/header.jsp" %>

<div class="container">
    <c:if test="${pbUser != null}">
        <liferay-portlet:renderURL var="homeLink">
            <portlet:param name="render" value=""/>
        </liferay-portlet:renderURL>
        <a href="${homeLink}" class="btn btn-info">
            <span class="glyphicon glyphicon-home"></span> Home
        </a>
    </c:if>

    <c:if test="${pbUser == null}">
        <button type="button" class="btn btn-info" disabled>
            <span class="glyphicon glyphicon-home"></span> Home
        </button>
    </c:if>

    <c:if test="${pbUser != null}">
        <tags:purposesTable purposes="${purposes}"/>

        <tags:addPurposeForm purpose="${purpose}"/>
    </c:if>

    <c:if test="${pbUser == null}">
        <h1>Please login to portal</h1>
    </c:if>
</div>

<%@include file="includes/footer.jsp" %>