<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0" %>
<%@ taglib prefix="liferay-portlet" uri="http://liferay.com/tld/portlet" %>
<%@ taglib prefix="liferay-util" uri="http://liferay.com/tld/util" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="aui" uri="http://liferay.com/tld/aui" %>

<portlet:defineObjects />

<style type="text/css">
    [ng\:cloak], [ng-cloak], .ng-cloak {
        display: none !important;
    }
</style>

<!-- Bootstrap CSS -->
<link href="${pageContext.request.contextPath}/lib/bootstrap-3.3.6-dist/css/bootstrap.css" rel="stylesheet">

<!-- Toasty -->
<link href="${pageContext.request.contextPath}/lib/angular-toasty/angular-toasty.css" rel="stylesheet">

<!-- Normalize -->
<link href="${pageContext.request.contextPath}/lib/normalize/normalize.css" rel="stylesheet">

<!-- My styles -->
<link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet">