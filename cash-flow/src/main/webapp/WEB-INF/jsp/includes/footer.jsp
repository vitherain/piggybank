<toasty></toasty>
</div>

<script>
    var cashFlowContextPath = "${pageContext.request.contextPath}";
    var cashFlowPortletNamespace = "<portlet:namespace />";
    var cashFlowWindowState = "${liferayPortletRequest.windowState.toString()}";

    //deletes unnecessary dashes
    var cashFlowPortletNamespaceTrimmed = cashFlowPortletNamespace.substring(1, cashFlowPortletNamespace.length - 1);
</script>
<script src="${pageContext.request.contextPath}/js/app.js"></script>
<script src="${pageContext.request.contextPath}/js/directives/isUnique.js"></script>
<script src="${pageContext.request.contextPath}/js/services/httpFacade.js"></script>
<script src="${pageContext.request.contextPath}/js/interceptors/errorHandlingHttpInterceptor.js"></script>
<script src="${pageContext.request.contextPath}/js/services/arrayUtils.js"></script>
<script src="${pageContext.request.contextPath}/js/services/modalService.js"></script>
<script src="${pageContext.request.contextPath}/js/controllers/CashFlowController.js"></script>
<script src="${pageContext.request.contextPath}/js/definePurposeCodeTable.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrapApp.js"></script>