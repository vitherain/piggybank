<%@tag description="Add or edit purpose" pageEncoding="UTF-8" %>
<%@ taglib prefix="aui" uri="http://liferay.com/tld/aui" %>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@attribute name="purpose" required="false" type="cz.herain.piggybank.common.dto.PurposeDTO" %>

<portlet:actionURL var="auiUrl" name="submitPurpose"/>
    <aui:form name="purpose" method="post" action="${auiUrl}">
        <aui:input type="hidden" name="id" value="${purpose.id}" />
        <aui:input type="hidden" name="pbUserId" value="${purpose.pbUserId == null ? pbUser.id : purpose.pbUserId}" />

        <div class="row">
            <div class="col-md-4 col-xs-12">
                Name
            </div>
            <div class="col-md-8 col-xs-12">
                <aui:input name="name" class="form-control" value="${purpose.name}" label="" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-xs-12">
                Is active
            </div>
            <div class="col-md-8 col-xs-12">
                <aui:input type="checkbox" name="active" checked="${purpose.active}" label="" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-xs-12">
                Is default
            </div>
            <div class="col-md-8 col-xs-12">
                <aui:input type="checkbox" name="defaultForCurrentUser" checked="${purpose.defaultForCurrentUser}"
                           disabled="${!purpose.active}" label="" />
            </div>
        </div>
        <div class="row">
            <button type="submit" class="btn-lg btn-primary">
                <c:choose>
                    <c:when test="${purpose.id == null || purpose.id == 0}">
                        Add
                    </c:when>
                    <c:otherwise>
                        Update
                    </c:otherwise>
                </c:choose>
            </button>
        </div>
    </aui:form>