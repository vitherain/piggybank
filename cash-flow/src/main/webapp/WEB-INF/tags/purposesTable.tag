<%@tag description="Table of purposes" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>

<%@attribute name="purposes" required="true" type="java.util.List" %>

<c:if test="${not empty purposes}">
    <table class="table table-striped table-hover inline-table scroll-overflow">
        <thead>
        <td class="col-md-3">
            <strong>Name</strong>
        </td>
        <td class="col-md-2">
            <strong>Active</strong>
        </td>
        <td class="col-md-2">
            <strong>Default</strong>
        </td>
        <td class="col-md-2">
            <strong>Operations</strong>
        </td>
        </thead>
        <c:forEach var="prps" items="${purposes}">
            <tr>
                <td>
                        ${prps.name}
                </td>
                <td>
                    <tags:booleanImage value="${prps.active}"/>
                </td>
                <td>
                    <tags:booleanImage value="${prps.defaultForCurrentUser}"/>
                </td>
                <td>
                    <c:choose>
                        <c:when test="${prps.active && !prps.defaultForCurrentUser}">
                            <portlet:actionURL var="setAsDefaultUrl" name="setPurposeAsDefault">
                                <portlet:param name="purposeId" value="${prps.id}"/>
                            </portlet:actionURL>
                            <a href="${setAsDefaultUrl}" title="Set as default">
                                <span class="glyphicon glyphicon-pushpin"></span>
                            </a>
                        </c:when>
                        <c:otherwise>
                            <span class="unavailable-glyphicon">
                                <span class="glyphicon glyphicon-pushpin"></span>
                            </span>
                        </c:otherwise>
                    </c:choose>

                    <portlet:renderURL var="editPurposeUrl">
                        <portlet:param name="render" value="editPurpose"/>
                        <portlet:param name="purposeId" value="${prps.id}"/>
                    </portlet:renderURL>
                    <a href="${editPurposeUrl}" title="Edit purpose">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </a>

                    <c:choose>
                        <c:when test="${prps.active eq true}">
                            <portlet:actionURL var="deactivatePurposeUrl" name="deactivatePurpose">
                                <portlet:param name="purposeId" value="${prps.id}"/>
                            </portlet:actionURL>
                            <a href="${deactivatePurposeUrl}" title="Deactivate purpose">
                                <span class="glyphicon glyphicon-remove"></span>
                            </a>
                        </c:when>
                        <c:otherwise>
                            <portlet:actionURL var="activatePurposeUrl" name="activatePurpose">
                                <portlet:param name="purposeId" value="${prps.id}"/>
                            </portlet:actionURL>
                            <a href="${activatePurposeUrl}" title="Activate purpose">
                                <span class="glyphicon glyphicon-ok"></span>
                            </a>
                        </c:otherwise>
                    </c:choose>
                </td>
            </tr>
        </c:forEach>
    </table>
</c:if>