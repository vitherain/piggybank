angular.module('piggy-bank').directive("isUnique", [function () {

    return {
        restrict: 'A',
        scope: {
            isUnique: '='
        },
        require: 'ngModel',
        link: function (scope, element, attrs, ctrl) {
            try {
                var knownValues = scope.isUnique;

                ctrl.$validators.isUnique = function (modelValue, viewValue) {
                    if (knownValues == undefined || modelValue == undefined) {
                        return true;
                    }

                    if (angular.isArray(knownValues) && modelValue) {
                        return knownValues.indexOf(modelValue) === -1;
                    }

                    return false;
                };
            } catch(e) {
                return true;
            }
        }
    };
}]);