package cz.herain.piggybank.common.service;

import java.util.List;

import cz.herain.piggybank.common.dto.AmountDTO;
import cz.herain.piggybank.common.dto.CostMonthCompleteDTO;
import cz.herain.piggybank.common.dto.CostMonthDTO;
import cz.herain.piggybank.common.service.base.BaseGetAllService;
import cz.herain.piggybank.common.service.base.BaseGetOneService;
import cz.herain.piggybank.common.service.base.BaseSaveOrUpdateOneService;
import cz.herain.piggybank.common.service.base.BaseSaveSeveralService;

public interface CostMonthService extends BaseGetAllService<CostMonthDTO>, BaseGetOneService<CostMonthDTO>,
        BaseSaveOrUpdateOneService<CostMonthDTO>, BaseSaveSeveralService<CostMonthDTO> {

    List<CostMonthDTO> getAllByCostYearIdSortedByMonthDesc(long userId);

    CostMonthCompleteDTO getCompleteDtoById(long id);

    AmountDTO getBalanceByMonthId(long monthId);

    AmountDTO getSumOfExpensesByPurposeIdAndMonthId(long purposeId, long monthId);

    AmountDTO getSumOfIncomesByPurposeIdAndMonthId(long purposeId, long monthId);

    List<CostMonthDTO> getAllSortedByYearAndMonthAsc(long userId);
}
