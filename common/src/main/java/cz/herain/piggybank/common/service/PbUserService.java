package cz.herain.piggybank.common.service;

import cz.herain.piggybank.common.service.base.BaseGetAllService;
import cz.herain.piggybank.common.service.base.BaseGetOneService;
import cz.herain.piggybank.common.service.base.BaseSaveOrUpdateOneService;
import cz.herain.piggybank.common.service.base.BaseSaveSeveralService;
import cz.herain.piggybank.common.dto.PbUserDTO;

public interface PbUserService extends BaseGetAllService<PbUserDTO>, BaseGetOneService<PbUserDTO>,
        BaseSaveOrUpdateOneService<PbUserDTO>, BaseSaveSeveralService<PbUserDTO> {

    PbUserDTO getByUserId(long userId);
}
