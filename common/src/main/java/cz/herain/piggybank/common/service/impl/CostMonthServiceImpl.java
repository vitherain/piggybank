package cz.herain.piggybank.common.service.impl;

import java.math.BigInteger;
import java.util.Comparator;
import java.util.List;

import cz.herain.piggybank.common.dto.AmountDTO;
import cz.herain.piggybank.common.dto.CostMonthCompleteDTO;
import cz.herain.piggybank.common.dto.CostMonthDTO;
import cz.herain.piggybank.common.dto.ExpenseDTO;
import cz.herain.piggybank.common.dto.IncomeDTO;
import cz.herain.piggybank.common.entity.CostMonth;
import cz.herain.piggybank.common.repository.CostMonthRepository;
import cz.herain.piggybank.common.service.CostMonthService;
import cz.herain.piggybank.common.service.ExpenseService;
import cz.herain.piggybank.common.service.IncomeService;
import cz.herain.piggybank.common.service.base.impl.AbstractCommonService;
import cz.herain.piggybank.common.util.ContextUtil;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(propagation = Propagation.REQUIRED)
@Service
public class CostMonthServiceImpl
        extends AbstractCommonService<CostMonthDTO, CostMonthDTO, CostMonthDTO, CostMonthDTO, CostMonth>
        implements CostMonthService {

    @Autowired
    private ContextUtil contextUtil;

    @Autowired
    private ExpenseService expenseService;

    @Autowired
    private IncomeService incomeService;

    @Autowired
    private CostMonthRepository costMonthRepository;

    @Autowired
    private MapperFacade mapper;

    public CostMonthServiceImpl() {
        super(CostMonthDTO.class, CostMonthDTO.class, CostMonthDTO.class, CostMonthDTO.class, CostMonth.class);
    }

    @Override
    public CostMonthDTO saveOrUpdate(final CostMonthDTO dto) {
        final long currentUserId = contextUtil.getCurrentPbUserId();
        dto.setPbUserId(currentUserId);

        CostMonth entity = mapper.map(dto, CostMonth.class);
        entity = costMonthRepository.save(entity);

        return mapper.map(entity, CostMonthDTO.class);
    }

    @Override
    public List<CostMonthDTO> getAllByCostYearIdSortedByMonthDesc(final long costYearId) {
        List<CostMonth> costMonths = costMonthRepository.findByCostYearIdSortedByMonthDesc(costYearId);
        return mapper.mapAsList(costMonths, CostMonthDTO.class);
    }

    @Override
    public CostMonthCompleteDTO getCompleteDtoById(final long id) {
        CostMonth costMonth = costMonthRepository.findOne(id);
        CostMonthCompleteDTO dto = mapper.map(costMonth, CostMonthCompleteDTO.class);

        BigInteger sumOfExpenses = expenseService.getSumOfExpensesByMonthId(id).getAmount();
        dto.setSumOfExpenses(sumOfExpenses);
        BigInteger sumOfIncomes = incomeService.getSumOfIncomesByMonthId(id).getAmount();
        dto.setSumOfIncomes(sumOfIncomes);
        BigInteger balance = sumOfIncomes.subtract(sumOfExpenses);
        dto.setBalance(balance);

        dto = sortExpensesAndIncomes(dto);

        return dto;
    }

    private CostMonthCompleteDTO sortExpensesAndIncomes(final CostMonthCompleteDTO dto) {
        List<ExpenseDTO> expenses = dto.getExpenses();
        expenses.sort(Comparator.comparing(ExpenseDTO::getDayInMonth));
        List<IncomeDTO> incomes = dto.getIncomes();
        incomes.sort(Comparator.comparing(IncomeDTO::getDayInMonth));

        return dto;
    }

    @Override
    public AmountDTO getBalanceByMonthId(final long monthId) {
        BigInteger amount = costMonthRepository.countBalanceByMonthId(monthId);
        return AmountDTO.AmountDTOBuilder.anAmountDTO()
                .withAmount(amount)
                .build();
    }

    @Override
    public AmountDTO getSumOfExpensesByPurposeIdAndMonthId(final long purposeId, final long monthId) {
        BigInteger amount = costMonthRepository.countSumOfExpensesByPurposeIdAndMonthId(purposeId, monthId);
        if (amount == null) {
            amount = new BigInteger("0");
        }

        return AmountDTO.AmountDTOBuilder.anAmountDTO()
                .withAmount(amount)
                .build();
    }

    @Override
    public AmountDTO getSumOfIncomesByPurposeIdAndMonthId(final long purposeId, final long monthId) {
        BigInteger amount = costMonthRepository.countSumOfIncomesByPurposeIdAndMonthId(purposeId, monthId);
        if (amount == null) {
            amount = new BigInteger("0");
        }

        return AmountDTO.AmountDTOBuilder.anAmountDTO()
                .withAmount(amount)
                .build();
    }

    @Override
    public List<CostMonthDTO> getAllSortedByYearAndMonthAsc(final long userId) {
        List<CostMonth> costMonths = costMonthRepository.findByUserIdSortedByYearAndMonthAsc(userId);
        return mapper.mapAsList(costMonths, CostMonthDTO.class);
    }
}
