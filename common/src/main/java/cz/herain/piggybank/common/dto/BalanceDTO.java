package cz.herain.piggybank.common.dto;

import java.io.Serializable;

/**
 * Created by Vít on 31. 10. 2016.
 */
public class BalanceDTO implements Serializable {

    private AmountDTO amount;
    private Long costYearId;
    private int costYearAsNumber;
    private Long costMonthId;
    private int costMonthMonthInYear;
    private int dayInMonth;

    public AmountDTO getAmount() {
        return amount;
    }

    public void setAmount(AmountDTO amount) {
        this.amount = amount;
    }

    public Long getCostMonthId() {
        return costMonthId;
    }

    public void setCostMonthId(Long costMonthId) {
        this.costMonthId = costMonthId;
    }

    public int getCostMonthMonthInYear() {
        return costMonthMonthInYear;
    }

    public void setCostMonthMonthInYear(int costMonthMonthInYear) {
        this.costMonthMonthInYear = costMonthMonthInYear;
    }

    public Long getCostYearId() {
        return costYearId;
    }

    public void setCostYearId(Long costYearId) {
        this.costYearId = costYearId;
    }

    public int getCostYearAsNumber() {
        return costYearAsNumber;
    }

    public void setCostYearAsNumber(int costYearAsNumber) {
        this.costYearAsNumber = costYearAsNumber;
    }

    public int getDayInMonth() {
        return dayInMonth;
    }

    public void setDayInMonth(int dayInMonth) {
        this.dayInMonth = dayInMonth;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BalanceDTO that = (BalanceDTO) o;

        if (costYearAsNumber != that.costYearAsNumber) return false;
        if (costMonthMonthInYear != that.costMonthMonthInYear) return false;
        if (dayInMonth != that.dayInMonth) return false;
        if (amount != null ? !amount.equals(that.amount) : that.amount != null) return false;
        if (costYearId != null ? !costYearId.equals(that.costYearId) : that.costYearId != null) return false;
        return costMonthId != null ? costMonthId.equals(that.costMonthId) : that.costMonthId == null;

    }

    @Override
    public int hashCode() {
        int result = amount != null ? amount.hashCode() : 0;
        result = 31 * result + (costYearId != null ? costYearId.hashCode() : 0);
        result = 31 * result + costYearAsNumber;
        result = 31 * result + (costMonthId != null ? costMonthId.hashCode() : 0);
        result = 31 * result + costMonthMonthInYear;
        result = 31 * result + dayInMonth;
        return result;
    }

    @Override
    public String toString() {
        return "BalanceDTO{" +
                "amount=" + amount +
                ", costYearId=" + costYearId +
                ", costYearAsNumber=" + costYearAsNumber +
                ", costMonthId=" + costMonthId +
                ", costMonthMonthInYear=" + costMonthMonthInYear +
                ", dayInMonth=" + dayInMonth +
                '}';
    }
}
