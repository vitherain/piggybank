package cz.herain.piggybank.common.dto;

import org.hibernate.validator.constraints.NotEmpty;

public class PurposeMiniDTO extends BaseDTO {

    @NotEmpty
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        PurposeMiniDTO that = (PurposeMiniDTO) o;

        return name != null ? name.equals(that.name) : that.name == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PurposeMiniDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
