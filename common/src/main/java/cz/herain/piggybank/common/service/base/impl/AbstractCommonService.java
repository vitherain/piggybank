package cz.herain.piggybank.common.service.base.impl;

import com.google.common.collect.Lists;
import cz.herain.piggybank.common.dto.BaseDTO;
import cz.herain.piggybank.common.entity.BaseEntity;
import cz.herain.piggybank.common.exception.UnexpectedException;
import cz.herain.piggybank.common.repository.BaseRepository;
import cz.herain.piggybank.common.service.base.*;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Transactional(propagation = Propagation.REQUIRED)
public abstract class AbstractCommonService<
        GET_ONE_DTO,
        GET_ALL_DTO,
        SAVE_OR_UPDATE_ONE_DTO extends BaseDTO,
        SAVE_SEVERAL_DTO extends BaseDTO,
        E extends BaseEntity>
        implements BaseGetAllService<GET_ALL_DTO>,
        BaseGetOneService<GET_ONE_DTO>,
        BaseSaveOrUpdateOneService<SAVE_OR_UPDATE_ONE_DTO>,
        BaseSaveSeveralService<SAVE_SEVERAL_DTO>,
        BaseDeleteService {

    private Class<GET_ONE_DTO> getOneDtoClass;
    private Class<GET_ALL_DTO> getAllDtoClass;
    private Class<SAVE_OR_UPDATE_ONE_DTO> saveOrUpdateOneDtoClass;
    private Class<SAVE_SEVERAL_DTO> saveSeveralDtoClass;
    private Class<E> entityClass;

    public AbstractCommonService(
            Class<GET_ONE_DTO> getOneDtoClass,
            Class<GET_ALL_DTO> getAllDtoClass,
            Class<SAVE_OR_UPDATE_ONE_DTO> saveOrUpdateOneDtoClass,
            Class<SAVE_SEVERAL_DTO> saveSeveralDtoClass,
            Class<E> entityClass) {
        this.getOneDtoClass = getOneDtoClass;
        this.getAllDtoClass = getAllDtoClass;
        this.saveOrUpdateOneDtoClass = saveOrUpdateOneDtoClass;
        this.saveSeveralDtoClass = saveSeveralDtoClass;
        this.entityClass = entityClass;
    }

    @Autowired
    private BaseRepository<E> repository;

    @Autowired
    private MapperFacade mapper;

    @Override
    public List<GET_ALL_DTO> getAll() {
        List<E> entities = repository.findAll();
        return mapper.mapAsList(entities, getAllDtoClass);
    }

    @Override
    public GET_ONE_DTO getOne(long id) {
        E entity = repository.findOne(id);
        return mapper.map(entity, getOneDtoClass);
    }

    @Override
    public SAVE_OR_UPDATE_ONE_DTO saveOrUpdate(SAVE_OR_UPDATE_ONE_DTO dto) {
        try {
            if (isNewRecord(dto)) {
                return save(dto);
            } else {
                return update(dto);
            }
        } catch (DataIntegrityViolationException e) {
            throw new UnexpectedException(e);
        }
    }

    private boolean isNewRecord(SAVE_OR_UPDATE_ONE_DTO dto) {
        return dto.getId() == null;
    }

    private SAVE_OR_UPDATE_ONE_DTO save(SAVE_OR_UPDATE_ONE_DTO dto) {
        E entity = mapper.map(dto, entityClass);
        return saveEntityAndReturnDTO(entity);
    }

    protected SAVE_OR_UPDATE_ONE_DTO saveEntityAndReturnDTO(E entity) {
        entity = repository.save(entity);
        return mapper.map(entity, saveOrUpdateOneDtoClass);
    }

    private SAVE_OR_UPDATE_ONE_DTO update(SAVE_OR_UPDATE_ONE_DTO dto) {
        E entity = repository.findOne(dto.getId());

        mapper.map(dto, entity);
        return saveEntityAndReturnDTO(entity);
    }

    @Override
    public List<SAVE_SEVERAL_DTO> save(Iterable<SAVE_SEVERAL_DTO> dtos) {
        List<E> saved = mapAndSaveAll(dtos);
        return mapper.mapAsList(saved, saveSeveralDtoClass);
    }

    private List<E> mapAndSaveAll(Iterable<SAVE_SEVERAL_DTO> dtos) {
        List<E> saved = new ArrayList<>();

        List<SAVE_SEVERAL_DTO> list = Lists.newArrayList(dtos);
        list.stream().forEach(dto -> {
            E entity = mapper.map(dto, entityClass);
            entity = repository.save(entity);
            saved.add(entity);
        });

        return saved;
    }

    @Override
    public void delete(long id) {
        E entity = repository.findOne(id);
        repository.delete(entity);
    }
}
