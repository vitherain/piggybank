package cz.herain.piggybank.common.service.impl;

import cz.herain.piggybank.common.dto.PurposeDTO;
import cz.herain.piggybank.common.dto.PurposeMiniDTO;
import cz.herain.piggybank.common.entity.PbUser;
import cz.herain.piggybank.common.entity.Purpose;
import cz.herain.piggybank.common.repository.PbUserRepository;
import cz.herain.piggybank.common.repository.PurposeRepository;
import cz.herain.piggybank.common.service.PurposeService;
import cz.herain.piggybank.common.service.base.impl.AbstractCommonService;
import cz.herain.piggybank.common.util.ContextUtil;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;

@Transactional(propagation = Propagation.REQUIRED)
@Service
public class PurposeServiceImpl
        extends AbstractCommonService<PurposeDTO, PurposeDTO, PurposeDTO, PurposeDTO, Purpose>
        implements PurposeService {

    @Autowired
    private ContextUtil contextUtil;

    @Autowired
    private PurposeRepository purposeRepository;

    @Autowired
    private PbUserRepository pbUserRepository;

    @Autowired
    private MapperFacade mapper;

    public PurposeServiceImpl() {
        super(PurposeDTO.class, PurposeDTO.class, PurposeDTO.class, PurposeDTO.class, Purpose.class);
    }

    @Override
    public List<PurposeDTO> getAll() {
        List<Purpose> purposes = purposeRepository.findAll();
        List<PurposeDTO> dtos = mapper.mapAsList(purposes, PurposeDTO.class);

        PbUser pbUser = getCurrentPbUser();
        for (int i = 0 ; i < purposes.size() ; i++) {
            if (purposes.get(i).equals(pbUser.getDefaultPurpose())) {
                dtos.get(i).setDefaultForCurrentUser(true);
                break;
            }
        }

        return dtos;
    }

    @Override
    public PurposeDTO getOne(long id) {
        Purpose purpose = purposeRepository.findOne(id);

        PbUser pbUser = getCurrentPbUser();
        return makeDTOFromModelForUser(purpose, pbUser);
    }

    private PurposeDTO makeDTOFromModelForUser(Purpose purpose, PbUser pbUser) {
        PurposeDTO dto = mapper.map(purpose, PurposeDTO.class);
        dto.setDefaultForCurrentUser(purpose.equals(pbUser.getDefaultPurpose()));

        return dto;
    }

    @Override
    public PurposeDTO saveOrUpdate(PurposeDTO dto) {
        Purpose purpose;

        if (dto.isNew()) {
            purpose = mapper.map(dto, Purpose.class);
        } else {
            purpose = purposeRepository.findOne(dto.getId());
            mapper.map(dto, purpose);
        }
        purpose = purposeRepository.save(purpose);
        setOrRemoveDefaultPurposeIfNecessary(dto, purpose);

        return mapper.map(purpose, PurposeDTO.class);
    }

    @Override
    public List<PurposeDTO> save(Iterable<PurposeDTO> purposeDTOs) {
        List<PurposeDTO> list = new LinkedList<>();

        for (PurposeDTO dto : purposeDTOs) {
            list.add(this.saveOrUpdate(dto));
        }

        return list;
    }

    private void setOrRemoveDefaultPurposeIfNecessary(final PurposeDTO dto, final Purpose purpose) {
        PbUser pbUser = purpose.getPbUser();

        if (dto.isDefaultForCurrentUser()) {
            setDefaultPurposeAndSaveUser(purpose, pbUser);
        } else if (pbUser.hasAsDefaultPurpose(purpose)) {
            setDefaultPurposeAndSaveUser(null, pbUser);
        }
    }

    private PbUser setDefaultPurposeAndSaveUser(final Purpose purpose, final PbUser pbUser) {
        pbUser.setDefaultPurpose(purpose);
        return pbUserRepository.save(pbUser);
    }

    private PbUser getCurrentPbUser() {
        long userId = contextUtil.getCurrentLiferayUserId();

        return pbUserRepository.findByUserId(userId);
    }

    @Override
    public List<PurposeDTO> getAllSortedByName() {
        List<Purpose> purposes = purposeRepository.findAll(new Sort("name"));

        return mapToDTOListAndSetDefault(purposes);
    }

    private List<PurposeDTO> mapToDTOListAndSetDefault(List<Purpose> purposes) {
        List<PurposeDTO> dtos = mapper.mapAsList(purposes, PurposeDTO.class);

        PbUser pbUser = getCurrentPbUser();
        for (int i = 0 ; i < purposes.size() ; i++) {
            if (purposes.get(i).equals(pbUser.getDefaultPurpose())) {
                dtos.get(i).setDefaultForCurrentUser(true);
                break;
            }
        }

        return dtos;
    }

    @Override
    public List<PurposeDTO> getByUserIdSortedByNameAsc(final long userId) {
        List<Purpose> purposes = purposeRepository.findByUserIdSortedByNameAsc(userId);

        return mapToDTOListAndSetDefault(purposes);
    }

    @Override
    public List<PurposeDTO> getActiveForCurrentUserSortedByNameAsc() {
        long userId = contextUtil.getCurrentPbUserId();
        List<Purpose> purposes = purposeRepository.findActiveByUserIdSortedByNameAsc(userId);

        return mapToDTOListAndSetDefault(purposes);
    }

    @Override
    public List<PurposeMiniDTO> getMinimalActiveByUserIdSortedByNameAsc(final long userId) {
        List<Purpose> purposes = purposeRepository.findMinimalActiveByUserIdSortedByNameAsc(userId);

        return mapper.mapAsList(purposes, PurposeMiniDTO.class);
    }

    @Override
    public List<PurposeMiniDTO> getMinimalByUserIdSortedByNameAsc(final long userId) {
        List<Purpose> purposes = purposeRepository.findMinimalByUserIdSortedByNameAsc(userId);

        return mapper.mapAsList(purposes, PurposeMiniDTO.class);
    }

    @Override
    public List<PurposeDTO> getCompleteActiveByUserIdSortedByNameAsc(final long userId) {
        List<Purpose> purposes = purposeRepository.findActiveByUserIdSortedByNameAsc(userId);

        return mapToDTOListAndSetDefault(purposes);
    }

    @Override
    public List<PurposeMiniDTO> getPurposesUsedForExpensesInMonth(final long monthId) {
        List<Purpose> purposes = purposeRepository.findPurposesUsedForExpensesInMonth(monthId);

        return mapper.mapAsList(purposes, PurposeMiniDTO.class);
    }

    @Override
    public List<PurposeMiniDTO> getPurposesUsedForIncomesInMonth(final long monthId) {
        List<Purpose> purposes = purposeRepository.findPurposesUsedForIncomesInMonth(monthId);

        return mapper.mapAsList(purposes, PurposeMiniDTO.class);
    }

    @Override
    public List<PurposeMiniDTO> getPurposesUsedForExpensesInMonths(final List<Long> costMonthIds) {
        List<Purpose> purposes = purposeRepository.findPurposesUsedForExpensesInMonths(costMonthIds);

        return mapper.mapAsList(purposes, PurposeMiniDTO.class);
    }

    @Override
    public List<PurposeMiniDTO> getPurposesUsedForIncomesInMonths(final List<Long> costMonthIds) {
        List<Purpose> purposes = purposeRepository.findPurposesUsedForIncomesInMonths(costMonthIds);

        return mapper.mapAsList(purposes, PurposeMiniDTO.class);
    }

    @Override
    public List<PurposeDTO> getActiveByUserIdSortedByNameAsc(long userId) {
        List<Purpose> purposes = purposeRepository.findActiveByUserIdSortedByNameAsc(userId);

        return mapToDTOListAndSetDefault(purposes);
    }

    @Override
    public List<PurposeDTO> getAllForCurrentUserSortedByNameAsc() {
        long userId = contextUtil.getCurrentPbUserId();
        List<Purpose> purposes = purposeRepository.findByUserIdSortedByNameAsc(userId);

        return mapToDTOListAndSetDefault(purposes);
    }
}
