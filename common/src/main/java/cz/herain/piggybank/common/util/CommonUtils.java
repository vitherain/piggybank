package cz.herain.piggybank.common.util;

import com.google.gson.Gson;

import javax.portlet.MimeResponse;
import javax.portlet.ResourceResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Vít on 13. 11. 2016.
 */
public class CommonUtils {

    public static  <T> void writeAsJson(final MimeResponse response, final T value) throws IOException {
        final Gson gson = new Gson();
        final String valueJson = gson.toJson(value);

        final PrintWriter out = response.getWriter();
        out.print(valueJson);
    }

    public static  <T> void setResourceResponseStatus(final ResourceResponse response, final int status) throws IOException {
        response.setProperty(ResourceResponse.HTTP_STATUS_CODE, String.valueOf(status));
    }
}
