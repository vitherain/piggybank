package cz.herain.piggybank.common.service;

import cz.herain.piggybank.common.dto.PurposeDTO;
import cz.herain.piggybank.common.dto.PurposeMiniDTO;
import cz.herain.piggybank.common.service.base.BaseGetAllService;
import cz.herain.piggybank.common.service.base.BaseGetOneService;
import cz.herain.piggybank.common.service.base.BaseSaveOrUpdateOneService;
import cz.herain.piggybank.common.service.base.BaseSaveSeveralService;

import java.util.List;

public interface PurposeService extends BaseGetAllService<PurposeDTO>, BaseGetOneService<PurposeDTO>,
        BaseSaveOrUpdateOneService<PurposeDTO>, BaseSaveSeveralService<PurposeDTO> {

    List<PurposeDTO> getAllSortedByName();

    List<PurposeDTO> getByUserIdSortedByNameAsc(long userId);

    List<PurposeDTO> getActiveForCurrentUserSortedByNameAsc();

    List<PurposeMiniDTO> getMinimalActiveByUserIdSortedByNameAsc(long userId);

    List<PurposeMiniDTO> getMinimalByUserIdSortedByNameAsc(long userId);

    List<PurposeDTO> getCompleteActiveByUserIdSortedByNameAsc(long userId);

    List<PurposeMiniDTO> getPurposesUsedForExpensesInMonth(long monthId);

    List<PurposeMiniDTO> getPurposesUsedForIncomesInMonth(long monthId);

    List<PurposeMiniDTO> getPurposesUsedForExpensesInMonths(List<Long> costMonthIds);

    List<PurposeMiniDTO> getPurposesUsedForIncomesInMonths(List<Long> costMonthIds);

    List<PurposeDTO> getActiveByUserIdSortedByNameAsc(long userId);

    List<PurposeDTO> getAllForCurrentUserSortedByNameAsc();
}
