package cz.herain.piggybank.common.repository;

import cz.herain.piggybank.common.entity.CostYear;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CostYearRepository extends BaseRepository<CostYear> {

    @Query("SELECT c FROM CostYear c WHERE c.pbUser.id = :userId ORDER BY c.year DESC")
    List<CostYear> findByUserIdSortedByYearDesc(@Param("userId") long userId);
}
