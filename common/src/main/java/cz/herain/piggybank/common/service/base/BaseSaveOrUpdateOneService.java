package cz.herain.piggybank.common.service.base;

import cz.herain.piggybank.common.dto.BaseDTO;

public interface BaseSaveOrUpdateOneService<DTO extends BaseDTO> {

    DTO saveOrUpdate(DTO dto);
}
