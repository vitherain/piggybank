package cz.herain.piggybank.common.util;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.User;
import com.liferay.portal.util.PortalUtil;
import cz.herain.piggybank.common.dto.PbUserDTO;
import cz.herain.piggybank.common.entity.PbUser;
import cz.herain.piggybank.common.repository.PbUserRepository;
import cz.herain.piggybank.common.service.PbUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.portlet.context.PortletRequestAttributes;

import javax.portlet.PortletRequest;
import java.util.Locale;

@Component
public class ContextUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(ContextUtil.class);

    @Autowired
    private PbUserService pbUserService;

    @Autowired
    private PbUserRepository pbUserRepository;

    public long getCurrentPbUserId() {
        try {
            RequestAttributes reqAttrs = RequestContextHolder.currentRequestAttributes();
            PortletRequest portletRequest = null;
            if (reqAttrs instanceof PortletRequestAttributes) {
                portletRequest = ((PortletRequestAttributes) reqAttrs).getRequest();
            }

            User currentUser = PortalUtil.getUser(portletRequest);

            if (currentUser != null) {
                return getPbUserForLiferayUser(currentUser).getId();
            }
        } catch (PortalException | SystemException e) {
            LOGGER.error("Error occurred during obtaining of current user {} | {}", e.getClass(), e.getMessage());
        }


        return 0;
    }

    private PbUserDTO getPbUserForLiferayUser(User user) {
        PbUserDTO pbUser = pbUserService.getByUserId(user.getUserId());

        if (pbUser == null) {
            pbUser = createAndSavePbUserForLiferayUser(user);
        }

        return pbUser;
    }

    private PbUserDTO createAndSavePbUserForLiferayUser(User user) {
        PbUserDTO pbUser = PbUserDTO.PbUserDTOBuilder
                .aPbUserDTO()
                .withUserId(user.getUserId())
                .build();

        return pbUserService.saveOrUpdate(pbUser);
    }

    public long getCurrentLiferayUserId() {
        RequestAttributes reqAttrs = RequestContextHolder.currentRequestAttributes();
        PortletRequest portletRequest = null;
        if (reqAttrs instanceof PortletRequestAttributes) {
            portletRequest = ((PortletRequestAttributes) reqAttrs).getRequest();
        }

        return portletRequest == null ? null : PortalUtil.getUserId(portletRequest);
    }

    public PbUser getCurrentPbUser() {
        long liferayUserId = this.getCurrentLiferayUserId();
        return pbUserRepository.findByUserId(liferayUserId);
    }

    public Locale getLocale() {
        RequestAttributes reqAttrs = RequestContextHolder.currentRequestAttributes();
        PortletRequest portletRequest = null;
        if (reqAttrs instanceof PortletRequestAttributes) {
            portletRequest = ((PortletRequestAttributes) reqAttrs).getRequest();
        }

        return portletRequest == null ? null : PortalUtil.getLocale(portletRequest);
    }
}
