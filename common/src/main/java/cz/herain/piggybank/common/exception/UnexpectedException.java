package cz.herain.piggybank.common.exception;

public class UnexpectedException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public UnexpectedException() {
		super();
	}

	public UnexpectedException(String message) {
		super(message);
	}

	public UnexpectedException(Throwable e) {
		super(e);
	}

	public UnexpectedException(String message, Throwable e) {
		super(message, e);
	}
}
