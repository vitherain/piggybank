package cz.herain.piggybank.common.service;

import cz.herain.piggybank.common.dto.AmountDTO;
import cz.herain.piggybank.common.dto.ExpenseDTO;
import cz.herain.piggybank.common.service.base.*;

import java.util.List;

public interface ExpenseService extends BaseGetAllService<ExpenseDTO>, BaseGetOneService<ExpenseDTO>,
        BaseSaveOrUpdateOneService<ExpenseDTO>, BaseSaveSeveralService<ExpenseDTO>,
        BaseDeleteService {

    List<ExpenseDTO> getByMonthIdOrderedByDayAsc(long monthId);

    AmountDTO getSumOfExpensesByMonthId(long monthId);

    List<AmountDTO> getAveragesByPurposeAndCostMonthsOrderedByPurposeName(List<Long> costMonthIds, long userId);

    AmountDTO getSumOfExpensesByPurposeIdAndMonthId(long purposeId, long monthId);

    AmountDTO getSumOfExpensesByPurposeIdAndMonthIds(long purposeId, List<Long> costMonthIds);

    AmountDTO getSumOfExpensesByMonthIds(List<Long> costMonthIds);
}
