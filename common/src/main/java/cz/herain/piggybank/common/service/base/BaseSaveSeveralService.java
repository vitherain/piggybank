package cz.herain.piggybank.common.service.base;

import cz.herain.piggybank.common.dto.BaseDTO;

import java.util.List;

public interface BaseSaveSeveralService<DTO extends BaseDTO> {

    List<DTO> save(Iterable<DTO> dtos);
}