package cz.herain.piggybank.common.dto;

/**
 * Created by Vit Herain on 03/11/2016.
 */
public class SumsByPurposeDTO extends BaseDTO {

    private String purposeName;
    private AmountDTO expensesAmount;
    private AmountDTO incomesAmount;

    public String getPurposeName() {
        return purposeName;
    }

    public void setPurposeName(final String purposeName) {
        this.purposeName = purposeName;
    }

    public AmountDTO getExpensesAmount() {
        return expensesAmount;
    }

    public void setExpensesAmount(final AmountDTO expensesAmount) {
        this.expensesAmount = expensesAmount;
    }

    public AmountDTO getIncomesAmount() {
        return incomesAmount;
    }

    public void setIncomesAmount(final AmountDTO incomesAmount) {
        this.incomesAmount = incomesAmount;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final SumsByPurposeDTO that = (SumsByPurposeDTO) o;

        if (purposeName != null ? !purposeName.equals(that.purposeName) : that.purposeName != null) return false;
        if (expensesAmount != null ? !expensesAmount.equals(that.expensesAmount) : that.expensesAmount != null) return false;
        return incomesAmount != null ? incomesAmount.equals(that.incomesAmount) : that.incomesAmount == null;

    }

    @Override
    public int hashCode() {
        int result = purposeName != null ? purposeName.hashCode() : 0;
        result = 31 * result + (expensesAmount != null ? expensesAmount.hashCode() : 0);
        result = 31 * result + (incomesAmount != null ? incomesAmount.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SumsByPurposeDTO{" +
                "id=" + id +
                ", purposeName='" + purposeName + '\'' +
                ", expensesAmount=" + expensesAmount +
                ", incomesAmount=" + incomesAmount +
                '}';
    }
}
