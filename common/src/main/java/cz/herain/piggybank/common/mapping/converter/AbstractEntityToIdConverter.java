package cz.herain.piggybank.common.mapping.converter;

import cz.herain.piggybank.common.entity.BaseEntity;
import cz.herain.piggybank.common.repository.BaseRepository;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractEntityToIdConverter<E extends BaseEntity, ID extends Long> extends BidirectionalConverter<E, ID> {

    @Autowired
    private BaseRepository<E> repository;

    @Override
    public ID convertTo(E source, Type<ID> destinationType) {
        if (source != null) {
            return (ID)source.getId();
        }

        return null;
    }

    @Override
    public E convertFrom(ID source, Type<E> destinationType) {
        if (source != null) {
            return repository.findOne(source);
        }

        return null;
    }
}
