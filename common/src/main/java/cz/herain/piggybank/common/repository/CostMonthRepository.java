package cz.herain.piggybank.common.repository;

import cz.herain.piggybank.common.entity.CostMonth;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigInteger;
import java.util.List;

public interface CostMonthRepository extends BaseRepository<CostMonth> {

    @Query("SELECT new CostMonth(cm.id, cm.monthInYear, cm.costYear) " +
            "FROM CostMonth cm " +
            "WHERE cm.id = :monthId")
    CostMonth findOneMinimal(@Param("monthId") long monthId);

    @Query("SELECT c FROM CostMonth c WHERE c.costYear.id = :costYearId ORDER BY c.monthInYear DESC")
    List<CostMonth> findByCostYearIdSortedByMonthDesc(@Param("costYearId") long costYearId);

    @Query(
            value = "SELECT inc.sum - exp.sum FROM "
                    + "(SELECT COALESCE(sum(amount), 0) sum FROM income where cost_month_id = :monthId) inc, "
                    + "(SELECT COALESCE(sum(amount), 0) sum FROM expense where cost_month_id = :monthId) exp",
            nativeQuery = true
    )
    BigInteger countBalanceByMonthId(@Param("monthId") long monthId);

    @Query(
            value = "SELECT inc.sum - exp.sum FROM "
                    + "(" +
                        "SELECT COALESCE(sum(amount), 0) sum " +
                        "FROM income " +
                        "WHERE cost_month_id = :monthId and day_in_month <= :dayInMonth) inc, "
                    + "(" +
                        "SELECT COALESCE(sum(amount), 0) sum " +
                        "FROM expense " +
                        "WHERE cost_month_id = :monthId and day_in_month <= :dayInMonth) exp",
            nativeQuery = true
    )
    BigInteger countBalanceByMonthIdAndDayInMonth(@Param("dayInMonth") long dayInMonth, @Param("monthId") long monthId);

    @Query("SELECT cm FROM CostMonth cm WHERE cm.costYear.pbUser.id = :userId ORDER BY cm.costYear.year ASC, cm.monthInYear ASC")
    List<CostMonth> findByUserIdSortedByYearAndMonthAsc(@Param("userId") long userId);

    @Query(
            value = "SELECT sum(amount) FROM expense WHERE purpose_id = :purposeId AND cost_month_id = :monthId",
            nativeQuery = true
    )
    BigInteger countSumOfExpensesByPurposeIdAndMonthId(@Param("purposeId") long purposeId, @Param("monthId") long monthId);

    @Query(
            value = "SELECT sum(amount) FROM income WHERE purpose_id = :purposeId AND cost_month_id = :monthId",
            nativeQuery = true
    )
    BigInteger countSumOfIncomesByPurposeIdAndMonthId(@Param("purposeId") long purposeId, @Param("monthId") long monthId);

    @Query("SELECT new CostMonth(cm.id, cm.monthInYear) " +
            "FROM CostMonth cm " +
            "WHERE cm.costYear.pbUser.id = :userId " +
            "ORDER BY cm.costYear.year ASC, cm.monthInYear ASC")
    List<CostMonth> findMinimalByUserIdSortedByYearAndMonthAsc(@Param("userId") long userId);

    @Query("SELECT cm.id " +
            "FROM CostMonth cm " +
            "WHERE cm.costYear.pbUser.id = :userId " +
            "ORDER BY cm.costYear.year ASC, cm.monthInYear ASC")
    List<Long> findIdsByUserIdSortedByYearAndMonthAsc(@Param("userId") long userId);
}
