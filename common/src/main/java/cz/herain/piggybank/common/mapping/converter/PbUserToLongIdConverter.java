package cz.herain.piggybank.common.mapping.converter;

import cz.herain.piggybank.common.entity.PbUser;
import org.springframework.stereotype.Component;

@Component
public class PbUserToLongIdConverter extends AbstractEntityToIdConverter<PbUser, Long> {
}
