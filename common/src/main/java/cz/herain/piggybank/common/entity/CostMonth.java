package cz.herain.piggybank.common.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "cost_month")
public class CostMonth extends BaseEntity {

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "cost_year_id", nullable = false)
    private CostYear costYear;

    @Column(name = "month_in_year", nullable = false)
    private int monthInYear;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "costMonth")
    private List<Expense> expenses;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "costMonth")
    private List<Income> incomes;

    public CostMonth() {
    }

    public CostMonth(final Long id, final int monthInYear) {
        super(id);
        this.monthInYear = monthInYear;
    }

    public CostMonth(final Long id, final int monthInYear, final CostYear costYear) {
        super(id);
        this.monthInYear = monthInYear;
        this.costYear = costYear;
    }

    public CostYear getCostYear() {
        return costYear;
    }

    public void setCostYear(CostYear costYear) {
        this.costYear = costYear;
    }

    public int getMonthInYear() {
        return monthInYear;
    }

    public void setMonthInYear(int monthInYear) {
        this.monthInYear = monthInYear;
    }

    public List<Expense> getExpenses() {
        return expenses;
    }

    public void setExpenses(List<Expense> expenses) {
        this.expenses = expenses;
    }

    public List<Income> getIncomes() {
        return incomes;
    }

    public void setIncomes(List<Income> incomes) {
        this.incomes = incomes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        CostMonth costMonth = (CostMonth) o;

        if (monthInYear != costMonth.monthInYear) return false;
        return costYear != null ? costYear.equals(costMonth.costYear) : costMonth.costYear == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (costYear != null ? costYear.hashCode() : 0);
        result = 31 * result + monthInYear;
        return result;
    }

    @Override
    public String toString() {
        return "CostMonth{" +
                "id=" + id +
                ", costYear=" + costYear.getYear() +
                ", monthInYear=" + monthInYear +
                '}';
    }
}
