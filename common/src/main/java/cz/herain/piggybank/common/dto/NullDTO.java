package cz.herain.piggybank.common.dto;

public final class NullDTO extends BaseDTO {

    private NullDTO() {
    }
}
