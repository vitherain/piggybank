package cz.herain.piggybank.common.exception;

import org.springframework.validation.Errors;

public class ValidationException extends RuntimeException {

	private static final long serialVersionUID = -6245659846514522203L;

	private Errors errors;

	public ValidationException() {
	}

	public ValidationException(Errors errors) {
		this.errors = errors;
	}

	public ValidationException(String message) {
		super(message);
	}

	public ValidationException(Throwable e) {
		super(e);
	}

	public ValidationException(String message, Throwable e) {
		super(message, e);
	}

	public ValidationException(String message, Throwable e, Errors errors) {
		super(message, e);
		this.errors = errors;
	}

	public Errors getErrors() {
		return errors;
	}

	public void setErrors(Errors errors) {
		this.errors = errors;
	}
}
