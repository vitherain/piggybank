package cz.herain.piggybank.common.dto;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

public class PurposeDTO extends BaseDTO {

    @NotEmpty
    private String name;

    private boolean active;

    private boolean defaultForCurrentUser;

    @NotNull
    private Long pbUserId;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDefaultForCurrentUser() {
        return defaultForCurrentUser;
    }

    public void setDefaultForCurrentUser(boolean defaultForCurrentUser) {
        this.defaultForCurrentUser = defaultForCurrentUser;
    }

    public Long getPbUserId() {
        return pbUserId;
    }

    public void setPbUserId(Long pbUserId) {
        this.pbUserId = pbUserId;
    }

    public boolean canBeSetAsDefault() {
        return !canNotBeSetAsDefault();
    }

    public boolean canNotBeSetAsDefault() {
        return isDefaultForCurrentUser() && !isActive();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        PurposeDTO that = (PurposeDTO) o;

        if (active != that.active) return false;
        if (defaultForCurrentUser != that.defaultForCurrentUser) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        return pbUserId != null ? pbUserId.equals(that.pbUserId) : that.pbUserId == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (active ? 1 : 0);
        result = 31 * result + (defaultForCurrentUser ? 1 : 0);
        result = 31 * result + (pbUserId != null ? pbUserId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return super.toString() +
                " PurposeDTO{" +
                "name='" + name + '\'' +
                ", active=" + active +
                ", defaultForCurrentUser=" + defaultForCurrentUser +
                ", pbUserId=" + pbUserId +
                '}';
    }
}
