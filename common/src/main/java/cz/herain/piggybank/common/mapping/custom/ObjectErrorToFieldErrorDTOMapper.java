package cz.herain.piggybank.common.mapping.custom;

import cz.herain.piggybank.common.dto.FieldErrorDTO;
import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MappingContext;
import org.springframework.stereotype.Component;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

@Component
public class ObjectErrorToFieldErrorDTOMapper extends CustomMapper<ObjectError, FieldErrorDTO> {

    @Override
    public void mapAtoB(ObjectError objectError, FieldErrorDTO fieldErrorDTO, MappingContext context) {
        if (objectError instanceof FieldError) {
            FieldError fieldError = (FieldError) objectError;
            fieldErrorDTO.setFieldName(fieldError.getField());
            fieldErrorDTO.setErrorType(fieldError.getCode());
            fieldErrorDTO.setRejectedValue(fieldError.getRejectedValue());
        }
    }
}
