package cz.herain.piggybank.common.service.impl;

import cz.herain.piggybank.common.dto.AmountDTO;
import cz.herain.piggybank.common.dto.ExpenseDTO;
import cz.herain.piggybank.common.entity.Expense;
import cz.herain.piggybank.common.repository.ExpenseRepository;
import cz.herain.piggybank.common.service.ExpenseService;
import cz.herain.piggybank.common.service.base.impl.AbstractCommonService;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

@Transactional(propagation = Propagation.REQUIRED)
@Service
public class ExpenseServiceImpl
        extends AbstractCommonService<ExpenseDTO, ExpenseDTO, ExpenseDTO, ExpenseDTO, Expense>
        implements ExpenseService {

    private static final String ZERO_STRING = "0";

    @Autowired
    private ExpenseRepository expenseRepository;

    @Autowired
    private MapperFacade mapper;

    public ExpenseServiceImpl() {
        super(ExpenseDTO.class, ExpenseDTO.class, ExpenseDTO.class, ExpenseDTO.class, Expense.class);
    }

    @Override
    public List<ExpenseDTO> getByMonthIdOrderedByDayAsc(long monthId) {
        List<Expense> expenses = expenseRepository.findByMonthIdSortedByDayDesc(monthId);
        return mapper.mapAsList(expenses, ExpenseDTO.class);
    }

    @Override
    public AmountDTO getSumOfExpensesByMonthId(long monthId) {
        BigInteger amount = expenseRepository.countSumAmountByCostMonthId(monthId);

        if (amount == null) {
            amount = new BigInteger(ZERO_STRING);
        }

        return AmountDTO.AmountDTOBuilder.anAmountDTO()
                .withAmount(amount)
                .build();
    }

    @Override
    public List<AmountDTO> getAveragesByPurposeAndCostMonthsOrderedByPurposeName(
            final List<Long> costMonthIds,
            final long userId) {

        final List<BigInteger> averages = expenseRepository
                .averagesByPurposeAndCostMonthsSortedByPurposeName(costMonthIds, costMonthIds.size(), userId);

        return averages.stream()
                .map((avg) -> AmountDTO.AmountDTOBuilder.anAmountDTO()
                        .withAmount(avg)
                        .build())
                .collect(Collectors.toList());
    }

    @Override
    public AmountDTO getSumOfExpensesByPurposeIdAndMonthId(final long purposeId, final long monthId) {
        BigInteger amount = expenseRepository.countSumByPurposeIdAndMonthId(purposeId, monthId);

        if (amount == null) {
            amount = new BigInteger(ZERO_STRING);
        }

        return AmountDTO.AmountDTOBuilder.anAmountDTO()
                .withAmount(amount)
                .build();
    }

    @Override
    public AmountDTO getSumOfExpensesByPurposeIdAndMonthIds(final long purposeId, final List<Long> costMonthIds) {
        BigInteger amount = expenseRepository.countSumByPurposeIdAndMonthIds(purposeId, costMonthIds);

        if (amount == null) {
            amount = new BigInteger(ZERO_STRING);
        }

        return AmountDTO.AmountDTOBuilder.anAmountDTO()
                .withAmount(amount)
                .build();
    }

    @Override
    public AmountDTO getSumOfExpensesByMonthIds(final List<Long> costMonthIds) {
        BigInteger amount = expenseRepository.countSumAmountByCostMonthIds(costMonthIds);

        if (amount == null) {
            amount = new BigInteger(ZERO_STRING);
        }

        return AmountDTO.AmountDTOBuilder.anAmountDTO()
                .withAmount(amount)
                .build();
    }
}
