package cz.herain.piggybank.common.entity;

import javax.persistence.*;

@Entity
@Table(name = "expense")
public class Expense extends BaseEntity {

    @Column(name = "day_in_month", nullable = false)
    private int dayInMonth;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "cost_month_id", nullable = false)
    private CostMonth costMonth;

    @ManyToOne(optional = true)
    @JoinColumn(name = "purpose_id", nullable = true)
    private Purpose purpose;

    @Column(nullable = true)
    private int amount;

    @Column(nullable = true)
    private String note;

    @Column(nullable = true)
    private String recipient;

    public int getDayInMonth() {
        return dayInMonth;
    }

    public void setDayInMonth(int dayInMonth) {
        this.dayInMonth = dayInMonth;
    }

    public CostMonth getCostMonth() {
        return costMonth;
    }

    public void setCostMonth(CostMonth costMonth) {
        this.costMonth = costMonth;
    }

    public Purpose getPurpose() {
        return purpose;
    }

    public void setPurpose(Purpose purpose) {
        this.purpose = purpose;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Expense expense = (Expense) o;

        if (dayInMonth != expense.dayInMonth) return false;
        if (amount != expense.amount) return false;
        if (costMonth != null ? !costMonth.equals(expense.costMonth) : expense.costMonth != null) return false;
        if (purpose != null ? !purpose.equals(expense.purpose) : expense.purpose != null) return false;
        if (note != null ? !note.equals(expense.note) : expense.note != null) return false;
        return recipient != null ? recipient.equals(expense.recipient) : expense.recipient == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + dayInMonth;
        result = 31 * result + (costMonth != null ? costMonth.hashCode() : 0);
        result = 31 * result + (purpose != null ? purpose.hashCode() : 0);
        result = 31 * result + amount;
        result = 31 * result + (note != null ? note.hashCode() : 0);
        result = 31 * result + (recipient != null ? recipient.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Expense{" +
                "id=" + id +
                ", amount=" + amount +
                ", dayInMonth=" + dayInMonth +
                ", costMonth=" + costMonth.getMonthInYear() +
                ", purposeId=" + (purpose != null ? purpose.getId() : null) +
                ", note='" + note + '\'' +
                ", recipient='" + recipient + '\'' +
                '}';
    }
}
