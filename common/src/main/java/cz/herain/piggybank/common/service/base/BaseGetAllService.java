package cz.herain.piggybank.common.service.base;

import java.util.List;

public interface BaseGetAllService<DTO> {

    List<DTO> getAll();
}
