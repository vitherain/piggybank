package cz.herain.piggybank.common.service.impl;

import cz.herain.piggybank.common.service.PbUserService;
import cz.herain.piggybank.common.service.base.impl.AbstractCommonService;
import cz.herain.piggybank.common.dto.PbUserDTO;
import cz.herain.piggybank.common.entity.PbUser;
import cz.herain.piggybank.common.repository.PbUserRepository;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(propagation = Propagation.REQUIRED)
@Service
public class PbUserServiceImpl
        extends AbstractCommonService<PbUserDTO, PbUserDTO, PbUserDTO, PbUserDTO, PbUser>
        implements PbUserService {

    @Autowired
    private PbUserRepository pbUserRepository;

    @Autowired
    private MapperFacade mapper;

    public PbUserServiceImpl() {
        super(PbUserDTO.class, PbUserDTO.class, PbUserDTO.class, PbUserDTO.class, PbUser.class);
    }

    @Override
    public PbUserDTO getByUserId(long userId) {
        PbUser pbUser = pbUserRepository.findByUserId(userId);
        return mapper.map(pbUser, PbUserDTO.class);
    }
}
