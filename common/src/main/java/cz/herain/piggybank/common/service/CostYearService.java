package cz.herain.piggybank.common.service;

import cz.herain.piggybank.common.service.base.BaseGetAllService;
import cz.herain.piggybank.common.service.base.BaseGetOneService;
import cz.herain.piggybank.common.service.base.BaseSaveOrUpdateOneService;
import cz.herain.piggybank.common.service.base.BaseSaveSeveralService;
import cz.herain.piggybank.common.dto.CostYearDTO;
import cz.herain.piggybank.common.dto.CostYearWithMonthsDTO;

import java.util.List;

public interface CostYearService extends BaseGetAllService<CostYearDTO>, BaseGetOneService<CostYearDTO>,
        BaseSaveOrUpdateOneService<CostYearDTO>, BaseSaveSeveralService<CostYearDTO> {

    List<CostYearDTO> getAllByUserIdSortedByYearDesc(long userId);

    List<CostYearWithMonthsDTO> getAllWithMonthsByUserIdSortedByYearDesc(long userId);
}
