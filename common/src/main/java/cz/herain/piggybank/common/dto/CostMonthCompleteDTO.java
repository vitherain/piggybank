package cz.herain.piggybank.common.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigInteger;
import java.util.List;

import static cz.herain.piggybank.common.util.ValidationConstants.MAX_MONTH;
import static cz.herain.piggybank.common.util.ValidationConstants.MIN_MONTH;

public class CostMonthCompleteDTO extends BaseDTO {

    @NotNull
    private Long costYearId;

    private int costYearAsNumber;

    @Min(value = MIN_MONTH)
    @Max(value = MAX_MONTH)
    private int monthInYear;

    private BigInteger balance;

    private List<ExpenseDTO> expenses;

    private BigInteger sumOfExpenses;

    private List<IncomeDTO> incomes;

    private BigInteger sumOfIncomes;

    public int getCostYearAsNumber() {
        return costYearAsNumber;
    }

    public void setCostYearAsNumber(int costYearAsNumber) {
        this.costYearAsNumber = costYearAsNumber;
    }

    public Long getCostYearId() {
        return costYearId;
    }

    public void setCostYearId(Long costYearId) {
        this.costYearId = costYearId;
    }

    public int getMonthInYear() {
        return monthInYear;
    }

    public void setMonthInYear(int monthInYear) {
        this.monthInYear = monthInYear;
    }

    public BigInteger getBalance() {
        return balance;
    }

    public void setBalance(BigInteger balance) {
        this.balance = balance;
    }

    public List<ExpenseDTO> getExpenses() {
        return expenses;
    }

    public void setExpenses(List<ExpenseDTO> expenses) {
        this.expenses = expenses;
    }

    public List<IncomeDTO> getIncomes() {
        return incomes;
    }

    public void setIncomes(List<IncomeDTO> incomes) {
        this.incomes = incomes;
    }

    public BigInteger getSumOfExpenses() {
        return sumOfExpenses;
    }

    public void setSumOfExpenses(BigInteger sumOfExpenses) {
        this.sumOfExpenses = sumOfExpenses;
    }

    public BigInteger getSumOfIncomes() {
        return sumOfIncomes;
    }

    public void setSumOfIncomes(BigInteger sumOfIncomes) {
        this.sumOfIncomes = sumOfIncomes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        CostMonthCompleteDTO that = (CostMonthCompleteDTO) o;

        if (costYearAsNumber != that.costYearAsNumber) return false;
        if (monthInYear != that.monthInYear) return false;
        if (costYearId != null ? !costYearId.equals(that.costYearId) : that.costYearId != null) return false;
        if (balance != null ? !balance.equals(that.balance) : that.balance != null) return false;
        if (sumOfExpenses != null ? !sumOfExpenses.equals(that.sumOfExpenses) : that.sumOfExpenses != null)
            return false;
        return sumOfIncomes != null ? sumOfIncomes.equals(that.sumOfIncomes) : that.sumOfIncomes == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (costYearId != null ? costYearId.hashCode() : 0);
        result = 31 * result + costYearAsNumber;
        result = 31 * result + monthInYear;
        result = 31 * result + (balance != null ? balance.hashCode() : 0);
        result = 31 * result + (sumOfExpenses != null ? sumOfExpenses.hashCode() : 0);
        result = 31 * result + (sumOfIncomes != null ? sumOfIncomes.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CostMonthCompleteDTO{" +
                "balance=" + balance +
                ", costYearId=" + costYearId +
                ", costYearAsNumber=" + costYearAsNumber +
                ", monthInYear=" + monthInYear +
                ", sumOfExpenses=" + sumOfExpenses +
                ", sumOfIncomes=" + sumOfIncomes +
                '}';
    }
}
