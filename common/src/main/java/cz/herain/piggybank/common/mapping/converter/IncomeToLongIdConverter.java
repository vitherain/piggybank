package cz.herain.piggybank.common.mapping.converter;

import cz.herain.piggybank.common.entity.Income;
import org.springframework.stereotype.Component;

@Component
public class IncomeToLongIdConverter extends AbstractEntityToIdConverter<Income, Long> {
}
