package cz.herain.piggybank.analytics.service;

import cz.herain.piggybank.common.dto.*;
import cz.herain.piggybank.common.dto.PercentagesByPurposeDTO.PercentagesByPurposeDTOBuilder;
import cz.herain.piggybank.common.entity.CostMonth;
import cz.herain.piggybank.common.repository.CostMonthRepository;
import cz.herain.piggybank.common.service.CostMonthService;
import cz.herain.piggybank.common.service.ExpenseService;
import cz.herain.piggybank.common.service.IncomeService;
import cz.herain.piggybank.common.service.PurposeService;
import cz.herain.piggybank.common.util.ContextUtil;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vit Herain on 03/11/2016.
 */
@Service
public class AnalyticServiceImpl implements AnalyticService {

    private static final int MIN_MONTH_DAY = 1;
    private static final int MAX_MONTH_DAY = 31;

    @Autowired
    private ContextUtil contextUtil;

    @Autowired
    private CostMonthRepository costMonthRepository;

    @Autowired
    private CostMonthService costMonthService;

    @Autowired
    private PurposeService purposeService;

    @Autowired
    private ExpenseService expenseService;

    @Autowired
    private IncomeService incomeService;

    @Autowired
    private MapperFacade mapper;

    @Override
    public List<BalanceDTO> getBalancesInIntervalOfCostMonths(final long monthFromId, final long monthToId) {
        final long userId = contextUtil.getCurrentPbUserId();
        final List<CostMonth> costMonths = costMonthRepository.findByUserIdSortedByYearAndMonthAsc(userId);

        final List<CostMonth> filteredCostMonths = filterCostMonths(costMonths, monthFromId, monthToId);

        final List<BalanceDTO> resultList = new ArrayList<>();

        for (final CostMonth cm : filteredCostMonths) {
            final BalanceDTO balanceDTO = mapper.map(cm, BalanceDTO.class);
            final AmountDTO balanceAmount = costMonthService.getBalanceByMonthId(cm.getId());
            balanceDTO.setAmount(balanceAmount);

            resultList.add(balanceDTO);
        }

        return resultList;
    }

    @Override
    public List<SumsByPurposeDTO> getPurposeSumsByCostMonthSortedByPurposeName(final long monthId) {
        final long userId = contextUtil.getCurrentPbUserId();
        final List<PurposeMiniDTO> purposes = purposeService.getMinimalActiveByUserIdSortedByNameAsc(userId);

        final List<SumsByPurposeDTO> resultList = new ArrayList<>();

        for (final PurposeMiniDTO purpose : purposes) {
            final  SumsByPurposeDTO dto = mapper.map(purpose, SumsByPurposeDTO.class);
            final AmountDTO sumOfExpenses = costMonthService.getSumOfExpensesByPurposeIdAndMonthId(purpose.getId(), monthId);
            final AmountDTO sumOfIncomes = costMonthService.getSumOfIncomesByPurposeIdAndMonthId(purpose.getId(), monthId);

            dto.setExpensesAmount(sumOfExpenses);
            dto.setIncomesAmount(sumOfIncomes);

            resultList.add(dto);
        }

        return resultList;
    }

    @Override
    public List<SumsByPurposeDTO> getPurposeAveragesInIntervalOfCostMonthsSortedByPurposeName(final long monthFromId, final long monthToId) {
        final long userId = contextUtil.getCurrentPbUserId();
        final List<Long> cmIds = costMonthRepository.findIdsByUserIdSortedByYearAndMonthAsc(userId);
        final List<Long> filteredCmIds = filterCostMonthIds(cmIds, monthFromId, monthToId);

        final List<PurposeMiniDTO> purposes = purposeService.getMinimalByUserIdSortedByNameAsc(userId);
        final List<AmountDTO> expenseAvgs = expenseService.getAveragesByPurposeAndCostMonthsOrderedByPurposeName(filteredCmIds, userId);
        final List<AmountDTO> incomeAvgs = incomeService.getAveragesByPurposeAndCostMonthsOrderedByPurposeName(filteredCmIds, userId);

        Assert.isTrue((purposes.size() == expenseAvgs.size()) && purposes.size() == incomeAvgs.size());
        final List<SumsByPurposeDTO> resultList = new ArrayList<>();

        final int size = purposes.size();
        for (int i = 0 ; i < size ; i++) {
            final PurposeMiniDTO purpose = purposes.get(i);
            final AmountDTO expenseAvg = expenseAvgs.get(i);
            final AmountDTO incomeAvg = incomeAvgs.get(i);

            final SumsByPurposeDTO dto = mapper.map(purpose, SumsByPurposeDTO.class);
            dto.setExpensesAmount(expenseAvg);
            dto.setIncomesAmount(incomeAvg);

            resultList.add(dto);
        }

        return resultList;
    }

    @Override
    public List<BalanceDTO> getDayBalancesInMonth(final long monthId) {
        final CostMonth costMonth = costMonthRepository.findOneMinimal(monthId);
        final int lengthOfMonth = getLengthOfMonth(costMonth);
        Assert.isTrue(lengthOfMonth >= MIN_MONTH_DAY && lengthOfMonth <= MAX_MONTH_DAY);

        final List<BalanceDTO> resultList = new ArrayList<>();
        for (int dayInMonth = 1 ; dayInMonth <= lengthOfMonth ; dayInMonth++) {
            final BalanceDTO balanceDTO = mapper.map(costMonth, BalanceDTO.class);
            final AmountDTO balanceAmount = getBalanceInOneDayByMonthId(dayInMonth, monthId);

            balanceDTO.setAmount(balanceAmount);
            balanceDTO.setDayInMonth(dayInMonth);

            resultList.add(balanceDTO);
        }

        return resultList;
    }

    private int getLengthOfMonth(final CostMonth costMonth) {
        final LocalDate date = LocalDate.of(costMonth.getCostYear().getYear(), costMonth.getMonthInYear(), MIN_MONTH_DAY);
        return date.lengthOfMonth();
    }

    @Override
    public AmountDTO getBalanceInOneDayByMonthId(final int dayInMonth, final long monthId) {
        BigInteger amount = costMonthRepository.countBalanceByMonthIdAndDayInMonth(dayInMonth, monthId);
        return AmountDTO.AmountDTOBuilder.anAmountDTO()
                .withAmount(amount)
                .build();
    }

    @Override
    public PercentagesByPurposeSummaryDTO getPercentagesByPurposeInMonth(final long monthId) {
        final PercentagesByPurposeSummaryDTO result = new PercentagesByPurposeSummaryDTO();

        final List<PurposeMiniDTO> purposesUsedForExpenses = purposeService.getPurposesUsedForExpensesInMonth(monthId);
        final List<PurposeMiniDTO> purposesUsedForIncomes = purposeService.getPurposesUsedForIncomesInMonth(monthId);

        final AmountDTO sumOfExpenses = expenseService.getSumOfExpensesByMonthId(monthId);
        final AmountDTO sumOfIncomes = incomeService.getSumOfIncomesByMonthId(monthId);

        List<PercentagesByPurposeDTO> expensePercentages = countExpensePercentages(monthId, purposesUsedForExpenses, sumOfExpenses);
        result.setExpenses(expensePercentages);

        List<PercentagesByPurposeDTO> incomePercentages = countIncomePercentages(monthId, purposesUsedForIncomes, sumOfIncomes);
        result.setIncomes(incomePercentages);

        return result;
    }

    @Override
    public PercentagesByPurposeSummaryDTO getAveragePercentagesByPurposeInIntervalOfCostMonths(long monthFromId, long monthToId) {
        final long userId = contextUtil.getCurrentPbUserId();
        final List<Long> cmIds = costMonthRepository.findIdsByUserIdSortedByYearAndMonthAsc(userId);
        final List<Long> filteredCmIds = filterCostMonthIds(cmIds, monthFromId, monthToId);

        final PercentagesByPurposeSummaryDTO result = new PercentagesByPurposeSummaryDTO();

        final List<PurposeMiniDTO> purposesUsedForExpenses = purposeService.getPurposesUsedForExpensesInMonths(filteredCmIds);
        final List<PurposeMiniDTO> purposesUsedForIncomes = purposeService.getPurposesUsedForIncomesInMonths(filteredCmIds);

        final AmountDTO sumOfExpenses = expenseService.getSumOfExpensesByMonthIds(filteredCmIds);
        final AmountDTO sumOfIncomes = incomeService.getSumOfIncomesByMonthIds(filteredCmIds);

        List<PercentagesByPurposeDTO> expensePercentages = countExpensePercentages(filteredCmIds, purposesUsedForExpenses, sumOfExpenses);
        result.setExpenses(expensePercentages);

        List<PercentagesByPurposeDTO> incomePercentages = countIncomePercentages(filteredCmIds, purposesUsedForIncomes, sumOfIncomes);
        result.setIncomes(incomePercentages);

        return result;
    }

    private List<PercentagesByPurposeDTO> countExpensePercentages(
            final List<Long> costMonthIds,
            final List<PurposeMiniDTO> purposesUsedForExpenses,
            final AmountDTO sumOfExpenses) {

        final List<PercentagesByPurposeDTO> percentagesList = new ArrayList<>();

        for (final PurposeMiniDTO purpose : purposesUsedForExpenses) {
            final AmountDTO purposeExpense = expenseService.getSumOfExpensesByPurposeIdAndMonthIds(purpose.getId(), costMonthIds);
            final BigDecimal percentageNumber = new BigDecimal(purposeExpense.getAmount())
                    .divide(new BigDecimal(sumOfExpenses.getAmount()), 2, RoundingMode.HALF_UP)
                    .multiply(BigDecimal.valueOf(100));

            final PercentagesByPurposeDTO expensePercentage = PercentagesByPurposeDTOBuilder.aPercentagesByPurposeDTO()
                    .withPurposeName(purpose.getName())
                    .withPercentage(percentageNumber)
                    .build();

            percentagesList.add(expensePercentage);
        }

        return percentagesList;
    }

    private List<PercentagesByPurposeDTO> countExpensePercentages(
            final long monthId,
            final List<PurposeMiniDTO> purposesUsedForExpenses,
            final AmountDTO sumOfExpenses) {

        final List<PercentagesByPurposeDTO> percentagesList = new ArrayList<>();

        for (final PurposeMiniDTO purpose : purposesUsedForExpenses) {
            final AmountDTO purposeExpense = expenseService.getSumOfExpensesByPurposeIdAndMonthId(purpose.getId(), monthId);
            final BigDecimal percentageNumber = new BigDecimal(purposeExpense.getAmount())
                    .divide(new BigDecimal(sumOfExpenses.getAmount()), 2, RoundingMode.HALF_UP)
                    .multiply(BigDecimal.valueOf(100));

            final PercentagesByPurposeDTO expensePercentage = PercentagesByPurposeDTOBuilder.aPercentagesByPurposeDTO()
                    .withPurposeName(purpose.getName())
                    .withPercentage(percentageNumber)
                    .build();

            percentagesList.add(expensePercentage);
        }

        return percentagesList;
    }

    private List<PercentagesByPurposeDTO> countIncomePercentages(
            final List<Long> costMonthIds,
            final List<PurposeMiniDTO> purposesUsedForIncomes,
            final AmountDTO sumOfIncomes) {

        final List<PercentagesByPurposeDTO> percentagesList = new ArrayList<>();

        for (final PurposeMiniDTO purpose : purposesUsedForIncomes) {
            final AmountDTO purposeIncome = incomeService.getSumOfIncomesByPurposeIdAndMonthIds(purpose.getId(), costMonthIds);
            final BigDecimal percentageNumber = new BigDecimal(purposeIncome.getAmount())
                    .divide(new BigDecimal(sumOfIncomes.getAmount()), 3, RoundingMode.HALF_UP)
                    .multiply(BigDecimal.valueOf(100));

            final PercentagesByPurposeDTO incomePercentage = PercentagesByPurposeDTOBuilder.aPercentagesByPurposeDTO()
                    .withPurposeName(purpose.getName())
                    .withPercentage(percentageNumber)
                    .build();

            percentagesList.add(incomePercentage);
        }

        return percentagesList;
    }

    private List<PercentagesByPurposeDTO> countIncomePercentages(
            final long monthId,
            final List<PurposeMiniDTO> purposesUsedForIncomes,
            final AmountDTO sumOfIncomes) {

        final List<PercentagesByPurposeDTO> percentagesList = new ArrayList<>();

        for (final PurposeMiniDTO purpose : purposesUsedForIncomes) {
            final AmountDTO purposeIncome = incomeService.getSumOfIncomesByPurposeIdAndMonthId(purpose.getId(), monthId);
            final BigDecimal percentageNumber = new BigDecimal(purposeIncome.getAmount())
                    .divide(new BigDecimal(sumOfIncomes.getAmount()), 3, RoundingMode.HALF_UP)
                    .multiply(BigDecimal.valueOf(100));

            final PercentagesByPurposeDTO incomePercentage = PercentagesByPurposeDTOBuilder.aPercentagesByPurposeDTO()
                    .withPurposeName(purpose.getName())
                    .withPercentage(percentageNumber)
                    .build();

            percentagesList.add(incomePercentage);
        }

        return percentagesList;
    }

    private List<CostMonth> filterCostMonths(
            final List<CostMonth> costMonths,
            final long monthFromId,
            final long monthToId) {

        final List<CostMonth> filteredCms = new ArrayList<>();
        boolean monthFromFetched = false;
        boolean monthToFetched = false;

        for (final CostMonth cm : costMonths) {
            if (cm.getId().equals(monthFromId)) {
                monthFromFetched = true;
            }

            if (cm.getId().equals(monthToId)) {
                filteredCms.add(cm);
                monthToFetched = true;
            }

            if (monthFromFetched && !monthToFetched) {
                filteredCms.add(cm);
            }
        }
        return filteredCms;
    }

    private List<Long> filterCostMonthIds(
            final List<Long> costMonthIds,
            final long monthFromId,
            final long monthToId) {

        final List<Long> filteredCmIds = new ArrayList<>();
        boolean monthFromFetched = false;
        boolean monthToFetched = false;

        for (final Long cmId : costMonthIds) {
            if (cmId.equals(monthFromId)) {
                monthFromFetched = true;
            }

            if (cmId.equals(monthToId)) {
                filteredCmIds.add(cmId);
                monthToFetched = true;
            }

            if (monthFromFetched && !monthToFetched) {
                filteredCmIds.add(cmId);
            }
        }
        return filteredCmIds;
    }
}
