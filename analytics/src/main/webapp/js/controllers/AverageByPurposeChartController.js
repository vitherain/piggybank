angular.module('piggy-bank-analytics').controller("AverageByPurposeChartController", ["$scope", "$q", "httpFacade", "toasty", "arrayUtils", "SumsByPurposeDTO",
    function($scope, $q, httpFacade, toasty, arrayUtils, SumsByPurposeDTO) {

        function reloadUsersCostPeriods() {
            httpFacade.getResource("usersCostMonths").then(function(response) {
                $scope.costMonths = response.data;
            });
        }
        reloadUsersCostPeriods();

        $scope.monthRangeChanged = function() {
            if ($scope.monthFrom && $scope.monthTo && monthToIsAfterMonthFromOrTheSame()) {
                httpFacade.getResource("usersAveragesByBurposeInCostMonth",
                    { monthFromId: $scope.monthFrom.id, monthToId: $scope.monthTo.id })
                    .then(function (response) {
                        return response.data.map(function (object) {
                            return new SumsByPurposeDTO(object);
                        });
                    }).then(function (purposeSums) {
                        prepareChartData(purposeSums);
                    });
            }
        };

        function monthToIsAfterMonthFromOrTheSame() {
            var yearFrom = $scope.monthFrom.costYearAsNumber;
            var monthFromMonth = $scope.monthFrom.monthInYear;
            var yearTo = $scope.monthTo.costYearAsNumber;
            var monthToMonth = $scope.monthTo.monthInYear;

            return yearTo > yearFrom || (yearTo === yearFrom && monthToMonth >= monthFromMonth);
        }

        function prepareChartData(data) {
            $scope.labels = arrayUtils.getSubArrayBySelectorIgnoreIfNull(data, function(item) {
                return item.show ? item.purposeName : null;
            });
            $scope.purposes = data;

            $scope.chartData = [
                arrayUtils.getSubArrayBySelectorIgnoreIfNull(data, function(item) {
                    return item.show ? item.incomesAmount.amount : null;
                }),
                arrayUtils.getSubArrayBySelectorIgnoreIfNull(data, function(item) {
                    return item.show ? item.expensesAmount.amount : null;
                })
            ];
        }

        $scope.refreshData = function() {
            prepareChartData($scope.purposes);
        };

        $scope.series = ['Incomes [CZK]', 'Expenses [CZK]'];

        $scope.datasetOverride = [
            {
                backgroundColor: "rgba(177,229,112,0.7)"
            },
            {
                backgroundColor: "rgba(255,99,132,0.7)"
            }
        ];
    }]);