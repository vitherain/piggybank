angular.module('piggy-bank-analytics', ['ui.bootstrap', 'angular-toasty', 'ngMessages', 'chart.js'])
    .config(["toastyConfigProvider", function (toastyConfigProvider) {
        var toastyConfig = {
            shake: false,
            clickToClose: true,
            timeout: 8000,
            sound: false
        };

        toastyConfigProvider.setConfig(toastyConfig);
    }])
    .config(["$provide", function($provide) {
        $provide.decorator('toasty', ["$delegate", function ($delegate) {
            var successFn = $delegate.success;

            function newSuccess(arg) {
                if (arg && !arg.title) {
                    arg.title = "Úspěch!";
                }

                successFn(arg);
            }

            $delegate.success = newSuccess;

            var errorFn = $delegate.error;

            function newError(arg) {
                if (arg && !arg.title) {
                    arg.title = "Chyba!";
                }

                errorFn(arg);
            }

            $delegate.error = newError;

            $delegate.unexpectedError = function () {
                $delegate.error({msg: "Došlo k neočekávané chybě"});
            };

            return $delegate;
        }])
    }])
    .config(["$httpProvider", function($httpProvider) {
        $httpProvider.interceptors.push('errorHandlingHttpInterceptor');
    }]);