angular.module('piggy-bank-analytics').service("httpFacade", ["$q", "$http", function($q, $http) {

    this.getResourceUrlAsync = function() {
        var deferred = $q.defer();

        AUI().ready('liferay-portlet-url', function() {
            var resourceUrl = Liferay.PortletURL.createResourceURL();
            resourceUrl.setPortletId(analyticsPortletNamespaceTrimmed);

            deferred.resolve(resourceUrl);
        });

        return deferred.promise;
    };

    this.getActionUrlAsync = function() {
        var deferred = $q.defer();

        AUI().ready('liferay-portlet-url', function() {
            var actionUrl = Liferay.PortletURL.createActionURL();
            actionUrl.reservedParams.p_auth = Liferay.authToken;
            actionUrl.setPortletId(analyticsPortletNamespaceTrimmed);
            actionUrl.setWindowState(analyticsWindowState);

            deferred.resolve(actionUrl);
        });

        return deferred.promise;
    };

    this.getResource = function(resourceId, parameters) {
        return this.getResourceUrlAsync().then(function(resourceUrl) {
            resourceUrl.setResourceId(resourceId);

            for (var param in parameters) {
                if(!parameters.hasOwnProperty(param)) continue;

                resourceUrl.setParameter(param, parameters[param]);
            }

            return $http({
                method: 'GET',
                url: resourceUrl.toString()
            });
        });
    };

    this.getAction = function(actionName, parameters) {
        return this.getActionUrlAsync().then(function(actionUrl) {
            actionUrl.setName(actionName);

            for (var param in parameters) {
                if(!parameters.hasOwnProperty(param)) continue;

                actionUrl.setParameter(param, parameters[param]);
            }

            return $http({
                method: 'GET',
                url: actionUrl.toString()
            });
        });
    };

    this.postAction = function(actionName, data, parameters) {
        return this.getActionUrlAsync().then(function(actionUrl) {
            actionUrl.setName(actionName);

            for (var param in parameters) {
                if(!parameters.hasOwnProperty(param)) continue;

                actionUrl.setParameter(param, parameters[param]);
            }

            return $http({
                method: 'POST',
                url: actionUrl.toString(),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                transformRequest: function(data) {
                    var result = '';
                    for (var prop in data) {
                        if(!data.hasOwnProperty(prop)) continue;

                        // request body params delimiter
                        if (result) {
                            result = result + '&'
                        }

                        result = result + analyticsPortletNamespace + prop + '=' + data[prop];
                    }
                    return result;
                },
                data: data
            });
        });
    };
}]);