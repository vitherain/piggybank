angular.module('piggy-bank-analytics').service("arrayUtils", function() {

    this.oneArrayContainsSubArray = function(array, subArray, arrayObjectPropertyName) {
        if (!array || !subArray) {
            return false;
        }

        var arraySortedCopy = angular.copy(array).sort();
        var subArraySortedCopy = angular.copy(subArray).sort();

        if (!arrayObjectPropertyName) {
            var arraySortedCopyAsString = arraySortedCopy.toString();
            var subArraySortedCopyAsString = subArraySortedCopy.toString();

            return arraySortedCopyAsString.indexOf(subArraySortedCopyAsString) > -1;
        }

        for (let subArrayItem of subArraySortedCopy) {
            if (angular.isFunction(subArrayItem[arrayObjectPropertyName])) {
                if (!this.containsObject(arraySortedCopy, item => item[arrayObjectPropertyName]() === subArrayItem[arrayObjectPropertyName]())) {
                    return false;
                }
            } else {
                if (!this.containsObject(arraySortedCopy, item => item[arrayObjectPropertyName] === subArrayItem[arrayObjectPropertyName])) {
                    return false;
                }
            }
        }

        return true;
    };

    this.containsObject = function(array, predicateFn) {
        var index = this.indexOfObjectInArray(array, predicateFn);

        return index > - 1;
    };

    this.getObjectFromArray = function(array, predicateFn) {
        var index = this.indexOfObjectInArray(array, predicateFn);

        if (index > -1 && index < array.length) {
            return array[index];
        }

        return {};
    };

    this.indexOfObjectInArray = function(array, predicateFn) {
        for (var i = 0 ; i < array.length ; i++) {
            if (predicateFn(array[i], i)) {
                return i;
            }
        }

        return -1;
    };

    this.removeObjectFromArray = function(array, predicateFn) {
        var index = this.indexOfObjectInArray(array, predicateFn);

        if (index > -1 && index < array.length) {
            array.splice(index, 1);
        }

        return array;
    };

    this.getSubArrayByElementsPropertyName = function(array, propertyName) {
        var subArray = [];

        angular.forEach(array, function(item) {
            var value = item[propertyName];
            if (value) {
                subArray.push(value);
            }
        });

        return subArray;
    };

    this.getSubArrayBySelector = function(array, selectorFn) {
        var subArray = [];

        for (var i = 0 ; i < array.length ; i++) {
            var item = array[i];
            subArray.push(selectorFn(item, i));
        }

        return subArray;
    };

    this.getSubArrayBySelectorIgnoreIfNull = function(array, selectorFn) {
        var subArray = [];

        for (var i = 0 ; i < array.length ; i++) {
            var item = array[i];
            var transformedItem = selectorFn(item, i);

            if (typeof transformedItem !== 'undefined' && transformedItem != null) {
                subArray.push(transformedItem);
            }
        }

        return subArray;
    };
});