<%@ tag description="Analytics portlet main menu" pageEncoding="UTF-8" %>
<%@ taglib prefix="liferay-portlet" uri="http://liferay.com/tld/portlet" %>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="col-xs-12">
	<c:if test="${pbUser != null}">
		<liferay-portlet:renderURL var="homeLink">
			<portlet:param name="render" value=""/>
		</liferay-portlet:renderURL>
		<a href="${homeLink}" class="btn btn-info">
			<span class="glyphicon glyphicon-home"></span> Home
		</a>

		<liferay-portlet:renderURL var="balancesChartLink">
			<portlet:param name="render" value="balancesChart"/>
		</liferay-portlet:renderURL>
		<a class="btn btn-primary" href="${balancesChartLink}">
			<span class="fa fa-bar-chart"></span> Balances
		</a>
		<liferay-portlet:renderURL var="sumsByPurposeChartLink">
			<portlet:param name="render" value="sumsByPurposeChart"/>
		</liferay-portlet:renderURL>
		<a class="btn btn-primary" href="${sumsByPurposeChartLink}">
			<span class="fa fa-bar-chart"></span> Sum by purpose
		</a>
		<liferay-portlet:renderURL var="averageByPurposeChartLink">
			<portlet:param name="render" value="averageByPurposeChart"/>
		</liferay-portlet:renderURL>
		<a class="btn btn-primary" href="${averageByPurposeChartLink}">
			<span class="glyphicon glyphicon-equalizer"></span> Average by purpose
		</a>
		<liferay-portlet:renderURL var="monthCashFlowChartLink">
			<portlet:param name="render" value="monthCashFlowChart"/>
		</liferay-portlet:renderURL>
		<a class="btn btn-primary" href="${monthCashFlowChartLink}">
			<span class="fa fa-line-chart"></span> Month cash flow
		</a>
		<liferay-portlet:renderURL var="percentagesByPurposeChartLink">
			<portlet:param name="render" value="percentagesByPurposeChart"/>
		</liferay-portlet:renderURL>
		<a class="btn btn-primary" href="${percentagesByPurposeChartLink}">
			<span class="fa fa-pie-chart"></span> Percentages by purpose
		</a>
		<liferay-portlet:renderURL var="averagePercentagesByPurposeChartLink">
			<portlet:param name="render" value="averagePercentagesByPurposeChart"/>
		</liferay-portlet:renderURL>
		<a class="btn btn-primary" href="${averagePercentagesByPurposeChartLink}">
			<span class="fa fa-pie-chart"></span> Average percentages by purpose
		</a>
	</c:if>

	<c:if test="${pbUser == null}">
		<button class="btn btn-info" type="button" disabled>
			<span class="glyphicon glyphicon-home"></span> Home
		</button>

		<button class="btn btn-primary" type="button" disabled>
			<span class="fa fa-bar-chart"></span> Balances
		</button>

		<button class="btn btn-primary" type="button" disabled>
			<span class="fa fa-bar-chart"></span> Sum by purpose
		</button>

		<button class="btn btn-primary" type="button" disabled>
			<span class="glyphicon glyphicon-equalizer"></span> Average by purpose
		</button>

		<button class="btn btn-primary" type="button" disabled>
			<span class="fa fa-line-chart"></span> Month cash flow
		</button>

		<button class="btn btn-primary" type="button" disabled>
			<span class="fa fa-pie-chart"></span> Percentages by purpose
		</button>

		<button class="btn btn-primary" type="button" disabled>
			<span class="fa fa-pie-chart"></span> Average percentages by purpose
		</button>
	</c:if>
</div>