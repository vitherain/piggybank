<%@include file="includes/header.jsp" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="tag" %>

<div class="container-fluid" ng-controller="SumsByPurposeChartController">
    <%--MENU--%>
    <tag:menu/>
    <%--END MENU--%>
    <c:if test="${pbUser != null}">
        <h2>Sums by purpose chart</h2>
        <strong>Select month:</strong>
        <select ng-model="costMonth" ng-change="monthChanged()"
                ng-options="month.id as month.monthInYear + '/' + month.costYearAsNumber for month in costMonths">
            <option value="" disabled selected >Choose month...</option>
        </select>

        <div class="panel panel-default" ng-if="chartData && chartData.length">
            <div class="panel-body">
                <canvas id="sumsByPurposeChart" class="chart chart-bar" chart-dataset-override="datasetOverride"
                        chart-data="chartData" chart-labels="labels" chart-series="series">
                </canvas>
            </div>
            <span class="col-xs-12">
                <span ng-repeat="prps in purposes">
                <input type="checkbox" ng-change="refreshData()" ng-model="prps.show" ng-checked="prps.show"> <strong>{{ prps.purposeName }}</strong>
            </span>&nbsp;&nbsp;&nbsp;&nbsp;
            </span>
        </div>
    </c:if>

    <c:if test="${pbUser == null}">
        <h1>Please login to portal</h1>
    </c:if>
</div>

<%@include file="includes/footer.jsp" %>